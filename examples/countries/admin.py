#-*- coding: utf-8 -*-
from django.contrib import admin
from adminext.admin import ModelAdminExt
from models import *

class CountryAdmin(ModelAdminExt):
    child_links = (
        ('countries.city', 'country'),
    )

class CityAdmin(ModelAdminExt):
    freeze_fields = ('country',)
    
admin.site.register(Country, CountryAdmin)
admin.site.register(City, CityAdmin)
