from django.db import models
from django.utils.translation import ugettext as _

class Country(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=3)
    
    class Meta:
        verbose_name_plural = _('Countries')
        
    def __unicode__(self):
        return self.name
    
    
class City(models.Model):
    country = models.ForeignKey(Country)
    name = models.CharField(max_length=100)
    is_capital = models.BooleanField(default=False)
    
    class Meta:
        verbose_name_plural = _('Cities')

    def __unicode__(self):
        return self.name
    
    