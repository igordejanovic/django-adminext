# -*- coding: utf-8 -*-
# AutoCompleteForeignKey django widget
# Uses modified jquery autocomplete plugin.
# Author: Igor R. Dejanovic, igor.dejanovic@gmail.com
# See LICENSE for licensing information.

from django import forms
from django.forms.widgets import flatatt
from django.utils.encoding import smart_unicode
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.simplejson import JSONEncoder
from django.conf import settings
from django.utils import simplejson
from django.http import HttpResponse, HttpResponseBadRequest


def autocomplete_fk_call(func):
    '''
    Decorator for server side ajax views.
    Decorated function will get all GET parameters  and should return
    model instance iterable (e.g. query object).
    '''
    def decorator(request, **kwargs):
        objs = []
        if request.method == "GET":
            for key,value in request.GET.items():
                key = str(key)
                if len(value)==1:
                    kwargs[key] = value[0]
                else:
                    kwargs[key] = value
            objs = [{'label': unicode(o),
                     'value': o.id,
                     'url': hasattr(o, 'get_absolute_url') and o.get_absolute_url() or ''}
                    for o in func(request, **kwargs)]
            return HttpResponse(simplejson.dumps(objs))
        else:
            return HttpResponseBadRequest()
    return decorator


class AutoCompleteForeignKey(forms.TextInput):

    class Media:
        URL = "%sadminext" % settings.STATIC_URL
        if settings.DEBUG:
            js = (   "%sjquery/jquery.js" % settings.STATIC_URL,
                     "%sjquery/ui/jquery.ui.core.js" % settings.STATIC_URL,
                     "%sjquery/ui/jquery.ui.widget.js" % settings.STATIC_URL,
                     "%sjquery/ui/jquery.ui.position.js" % settings.STATIC_URL,
                     "%s/autocompletefk/jquery.ui.autocompleteforeignkey.js" % URL,
                     "%s/autocompletefk/acfk_RelatedObjectLookups.js" % URL,                    
            )
        else:
            js = (  "%sjquery/jquery.min.js" % settings.STATIC_URL,
                    "%sjquery/ui/minified/jquery.ui.core.min.js" % settings.STATIC_URL,
                    "%sjquery/ui/minified/jquery.ui.widget.min.js" % settings.STATIC_URL,
                    "%sjquery/ui/minified/jquery.ui.position.min.js" % settings.STATIC_URL,
                    "%s/autocompletefk/jquery.ui.autocompleteforeignkey.js" % URL,
                    "%s/autocompletefk/acfk_RelatedObjectLookups.js" % URL,                    
            )
            
        # Styling
        css = { 'all':
            ("%sjquery/themes/current/jquery.ui.core.css" % settings.STATIC_URL,
             "%sjquery/themes/current/jquery.ui.theme.css" % settings.STATIC_URL, 
             "%sjquery/themes/current/jquery.ui.autocomplete.css" % settings.STATIC_URL,
             "%s/autocompletefk/autocomplete.css" % URL)
        }

    def __init__(self, model, source=None, parent=None,
                 add_link=None, options={}, attrs={}):
        """
        @model      - referenced model
        @source     - url of the ajax view decorated with autocomplete_fk_call
        @parent     - parent field name or object id
        @add_link   - url for adding another model instance
        
        For jquery autocomplete plugin docs see:
        http://docs.jquery.com/Plugins/autocomplete
        """
        self.model = model
        self.attrs = attrs
        self.source = source
        self.parent = parent
        self.add_link = add_link
        self.options = options

        self.params = []
        if self.source:
            if isinstance(self.source, str):
                source = "'%s'" % escape(self.source)
                self.params.append("source:%s" % source)
            else:
                raise ValueError('source type is not valid')

            if isinstance(self.parent, str): # parent is field name
                self.params.append("parent_id:'id_%s'" % self.parent)
            elif parent: # parent is object id
                self.params.append("parent_id:%d" % self.parent)
            
        if len(options) > 0:
            self.options = JSONEncoder().encode(options)
            
        #self.attrs.update(attrs)
    
    def render_js(self, field_id):
        
        options = ''
        if self.options:
            options += ',%s' % self.options

        params = ', '.join(self.params)
        
        parent_change_handler = ''
        if isinstance(self.parent,str): # For on-form parent field
            parent_change_handler = '''
            $(function($){
                $('#add_%(field_id)s').data('url_template', '%(url_template)s')
                clear_fn = function(){
                  $('#lookup_%(field_id)s').val("");
                  $('#lookup_%(field_id)s').removeClass('autocomplete_fk-error');
                  $('#%(field_id)s').val("").change();                  
                  update_add_link('add_%(field_id)s', '%(dep_field_id)s');
                };
                $('#%(dep_field_id)s').change(clear_fn);
                update_add_link('add_%(field_id)s', '%(dep_field_id)s');                
            });
            ''' % {'field_id': field_id,
                   'dep_field_id': "id_%s" % self.parent,
                   'url_template': self.add_link or ''}
        
        return u'''$('#lookup_%(field_id)s').autocomplete_fk({%(params)s}%(options)s);
                    $('#%(field_id)s').change(function(){
                        var link_node = $('#link_%(field_id)s');
                        link_node.toggle($(this).val()!="" && link_node.attr('href')!="");
                    }).change();
                    %(parent_change_handler)s
            ''' % {
                'field_id': field_id,
                'params': params,
                'options': options,
                'parent_change_handler': parent_change_handler,
            }

    def render(self, name, value=None, attrs=None):
        final_attrs = self.build_attrs(attrs, name=name)

        if not self.attrs.has_key('id'):
            final_attrs['id'] = 'id_%s' % name
         
        lookup_value = ''
        lookup_url = ''
        if value:
            final_attrs['value'] = escape(smart_unicode(value))
            obj = self.model.objects.get(pk=value)
            lookup_value = escape(unicode(obj))
            if hasattr(obj, 'get_absolute_url'):
                lookup_url = escape(obj.get_absolute_url())

        disable_url = ''   
        if not lookup_url:
            disable_url = 'style="display:none;"'
            
        add_link_html = ''
        if self.add_link:
            add_link_html = u'''
                <a href="%(add_link)s" class="add-another" id="add_id_%(name)s"
                    onclick="return showAddAnotherPopup(this);">
                    <img src="%(admin_media)simg/admin/icon_addlink.gif"
                        width="10" height="10" alt="Додај још један"/>
                </a>
            ''' % {
                'add_link': self.add_link,
                'name': name,
                'admin_media': settings.ADMIN_MEDIA_PREFIX
            }
            
        js = ''
        if self.source:
            js = u'''<script type="text/javascript"><!--//
                    %s//--></script>''' % self.render_js(final_attrs['id'])

        
        return mark_safe(u'''<input type="hidden" %(attrs)s/>
        <input type="text" id="lookup_id_%(name)s" class="vTextField" value="%(lookup_value)s"/>
        <a id="link_id_%(name)s" class="autocomplete_fk-link" target="_blank" href="%(lookup_url)s" %(disable_url)s></a>
        %(add_link_html)s
        %(js)s
        ''' % {
            'name': name,
            'attrs' : flatatt(final_attrs),
            'add_link_html': add_link_html,
            'js' : js,
            'lookup_value': lookup_value,
            'lookup_url': lookup_url,
            'disable_url': disable_url,
        })
